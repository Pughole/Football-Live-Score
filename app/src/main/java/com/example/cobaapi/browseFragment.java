package com.example.cobaapi;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class browseFragment extends Fragment {


    RecyclerView rv;

    public browseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_browse, container, false);



        if(((MainActivity) getActivity()).listLeague.size()==0){
            ((MainActivity) getActivity()).addLeagueList();

        }
        rv = (RecyclerView) v.findViewById(R.id.rvBrowse);
        ArrayList<league> listLeague = ((MainActivity) getActivity()).listLeague;
        //Toast.makeText(getContext(), "isi League baru : " + listLeague.size(), Toast.LENGTH_SHORT).show();
        leagueAdapter adapter = new leagueAdapter(listLeague);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rv.setLayoutManager(layoutManager);
        rv.setAdapter(adapter);

        adapter.setOnItemClickCallback(new leagueAdapter.OnItemClickCallback() {
            @Override
            public void onClick(league item) {
                ((MainActivity) getActivity()).loadTeam(Integer.parseInt(item.getIdLeague()));
            }
        });


        return v;
    }

}
