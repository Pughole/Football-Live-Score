package com.example.cobaapi;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BackgroundService extends Service {
    public Context context = this;
    public Handler handler = null;
    public static Runnable runnable = null;
    ArrayList<TeamItem>listFavNotified = new ArrayList<TeamItem>();
    ArrayList<TeamItem>listFav = new ArrayList<TeamItem>();
    RequestQueue rq;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        handler = new Handler();
        runnable = new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void run() {
                checkFav();
                handler.postDelayed(runnable, 120000);
            }
        };
        handler.postDelayed(runnable, 120000);
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showNotif(String team){
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder b = new NotificationCompat.Builder(this,"1");

        b.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_live_tv_black_24dp)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setTicker("Hearty365")
                .setContentTitle("Live Score")
                .setContentText(team+" sedang bertanding sekarang!")
                .setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_SOUND)
                .setContentIntent(contentIntent)
                .setContentInfo("Info");


        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        String channelId = "Your_channel_id";
        NotificationChannel channel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            channel = new NotificationChannel(
                    channelId,
                    "Live Score",
                    NotificationManager.IMPORTANCE_HIGH);
        }
        //notificationManager.createNotificationChannel(channel);
        b.setChannelId(channelId);

        notificationManager.notify(1, b.build());

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void checkFav(){

        String url="https://www.thesportsdb.com/api/v1/json/4013017/latestsoccer.php";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray jsonArray = object.getJSONObject("teams").getJSONArray("Match");
                    for (int i=0;i<jsonArray.length();i++){
                        for(int j=0;j<listFav.size();j++){
                            if(jsonArray.getJSONObject(i).getInt("HomeTeam_Id")==listFav.get(j).getId()){
                                if(!listFavNotified.contains(listFav.get(j))){
                                    listFavNotified.add(listFav.get(j));
                                    showNotif(listFav.get(j).getNama());
                                }
                            }
                            if(jsonArray.getJSONObject(i).getInt("AwayTeam_Id")==listFav.get(j).getId()){
                                if(!listFavNotified.contains(listFav.get(j))){
                                    listFavNotified.add(listFav.get(j));
                                    showNotif(listFav.get(j).getNama());
                                }
                            }
                        }
                    }

                } catch (JSONException e) {

                    try{
                        JSONObject object = new JSONObject(response);
                        JSONObject jsonObject = object.getJSONObject("teams").getJSONObject("Match");
                        for(int j=0;j<listFav.size();j++){
                            if(jsonObject.getInt("HomeTeam_Id")==listFav.get(j).getId()){
                                if(!listFavNotified.contains(listFav.get(j))){
                                    listFavNotified.add(listFav.get(j));
                                    showNotif(listFav.get(j).getNama());
                                }
                            }
                            if(jsonObject.getInt("AwayTeam_Id")==listFav.get(j).getId()){
                                if(!listFavNotified.contains(listFav.get(j))){
                                    listFavNotified.add(listFav.get(j));
                                    showNotif(listFav.get(j).getNama());
                                }
                            }
                        }

                    }catch (Exception e1){
                        e1.printStackTrace();
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        rq = Volley.newRequestQueue(this);
        rq.add(stringRequest);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try{
            listFav=intent.getParcelableArrayListExtra("listFav");
        }catch (Exception e){

        }

        return super.onStartCommand(intent, flags, startId);
    }
}
