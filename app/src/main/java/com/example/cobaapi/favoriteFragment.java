package com.example.cobaapi;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class favoriteFragment extends Fragment {

    RecyclerView rv;
    ArrayList<TeamItem> listFav = new ArrayList<TeamItem>();
    AdapterRvTeams adapter;
    public favoriteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_favorite, container, false);

        rv = (RecyclerView) v.findViewById(R.id.rv_favorite);

        for(int i=0;i<((MainActivity) getActivity()).listFav.size();i++){
            if(((MainActivity) getActivity()).listFav.get(i).getUsername().equals(((MainActivity) getActivity()).currUser.getUsername())){
                listFav.add(((MainActivity) getActivity()).listFav.get(i));
            }
        }

        if(listFav.size()>0){
            adapter = new AdapterRvTeams(listFav);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
            rv.setLayoutManager(layoutManager);
            rv.setAdapter(adapter);

            adapter.setOnItemClickCallback(new AdapterRvTeams.OnItemClickCallback() {
                @Override
                public void onClick(TeamItem item) {
                    ((MainActivity) getActivity()).currTeam=item;
                    ((MainActivity) getActivity()).openFragment(new DetailTeamFragment());

                }
            });
        }




        return v;
    }

}
