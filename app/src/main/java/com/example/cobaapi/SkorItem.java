package com.example.cobaapi;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class SkorItem implements Parcelable {
    private String homeName,awayName,tanggal,league;
    private int homeSkor,awaySkor,id,idHome,idAway;
    private String logoHome,logoAway,time;
    private int shotsHome,shotsAway,yellowHome,yellowAway,redHome,redAway;
    private String winner;

    protected SkorItem(Parcel in) {
        homeName = in.readString();
        awayName = in.readString();
        tanggal = in.readString();
        league = in.readString();
        homeSkor = in.readInt();
        awaySkor = in.readInt();
        id = in.readInt();
        idHome = in.readInt();
        idAway = in.readInt();
        logoHome = in.readString();
        logoAway = in.readString();
        time = in.readString();
        shotsHome = in.readInt();
        shotsAway = in.readInt();
        yellowHome = in.readInt();
        yellowAway = in.readInt();
        redHome = in.readInt();
        redAway = in.readInt();
        winner = in.readString();
    }

    public static final Creator<SkorItem> CREATOR = new Creator<SkorItem>() {
        @Override
        public SkorItem createFromParcel(Parcel in) {
            return new SkorItem(in);
        }

        @Override
        public SkorItem[] newArray(int size) {
            return new SkorItem[size];
        }
    };

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdHome() {
        return idHome;
    }

    public void setIdHome(int idHome) {
        this.idHome = idHome;
    }

    public int getIdAway() {
        return idAway;
    }

    public void setIdAway(int idAway) {
        this.idAway = idAway;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getLogoHome() {
        return logoHome;
    }

    public void setLogoHome(String logoHome) {
        this.logoHome = logoHome;
    }

    public String getLogoAway() {
        return logoAway;
    }

    public void setLogoAway(String logoAway) {
        this.logoAway = logoAway;
    }

    public int getShotsHome() {
        return shotsHome;
    }

    public void setShotsHome(int shotsHome) {
        this.shotsHome = shotsHome;
    }

    public int getShotsAway() {
        return shotsAway;
    }

    public void setShotsAway(int shotsAway) {
        this.shotsAway = shotsAway;
    }

    public int getYellowHome() {
        return yellowHome;
    }

    public void setYellowHome(int yellowHome) {
        this.yellowHome = yellowHome;
    }

    public int getYellowAway() {
        return yellowAway;
    }

    public void setYellowAway(int yellowAway) {
        this.yellowAway = yellowAway;
    }

    public int getRedHome() {
        return redHome;
    }

    public void setRedHome(int redHome) {
        this.redHome = redHome;
    }

    public int getRedAway() {
        return redAway;
    }

    public void setRedAway(int redAway) {
        this.redAway = redAway;
    }

    public SkorItem(JSONObject object, int tipe){
        if(tipe==0){
            try{
                id=object.getInt("idEvent");
                homeName=object.getString("strHomeTeam");
                awayName=object.getString("strAwayTeam");
                homeSkor=object.getInt("intHomeScore");
                awaySkor=object.getInt("intAwayScore");

                if(homeSkor>awaySkor){
                    winner="HOME_TEAM";
                }else if(awaySkor>homeSkor){
                    winner="AWAY_TEAM";
                }else{
                    winner="DRAW";
                }

                league = object.getString("strLeague");
                tanggal = object.getString("dateEvent");
                idHome=object.getInt("idHomeTeam");
                idAway=object.getInt("idAwayTeam");

            }catch(Exception e){

            }
        }else if(tipe==1){
            try{
                id=object.getInt("idEvent");
                homeName=object.getString("strHomeTeam");
                awayName=object.getString("strAwayTeam");
                league = object.getString("strLeague");
                tanggal = object.getString("dateEvent");

            }catch(Exception e){

            }
        }else if(tipe==2){
            try {
                homeName=object.getString("HomeTeam");
                awayName=object.getString("AwayTeam");
                homeSkor=object.getInt("HomeGoals");
                awaySkor=object.getInt("AwayGoals");
                league = object.getString("League");
                String a = object.getString("Date");
                SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'+'MM:SS");
                SimpleDateFormat destFormat = new SimpleDateFormat("HH.mm");
                Date date = null;
                sourceFormat.setTimeZone(TimeZone.getTimeZone("Europe/London"));
                try {
                    date = sourceFormat.parse(a);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                destFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
                tanggal = destFormat.format(date);
                idHome=object.getInt("HomeTeam_Id");
                idAway=object.getInt("AwayTeam_Id");
                time=object.getString("Time");
            }catch (Exception e){
            }
        }
    }

    public String getHomeName() {
        return homeName;
    }

    public void setHomeName(String homeName) {
        this.homeName = homeName;
    }

    public String getAwayName() {
        return awayName;
    }

    public void setAwayName(String awayName) {
        this.awayName = awayName;
    }

    public int getHomeSkor() {
        return homeSkor;
    }

    public void setHomeSkor(int homeSkor) {
        this.homeSkor = homeSkor;
    }

    public int getAwaySkor() {
        return awaySkor;
    }

    public void setAwaySkor(int awaySkor) {
        this.awaySkor = awaySkor;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(homeName);
        dest.writeString(awayName);
        dest.writeString(tanggal);
        dest.writeString(league);
        dest.writeInt(homeSkor);
        dest.writeInt(awaySkor);
        dest.writeInt(id);
        dest.writeInt(idHome);
        dest.writeInt(idAway);
        dest.writeString(logoHome);
        dest.writeString(logoAway);
        dest.writeString(time);
        dest.writeInt(shotsHome);
        dest.writeInt(shotsAway);
        dest.writeInt(yellowHome);
        dest.writeInt(yellowAway);
        dest.writeInt(redHome);
        dest.writeInt(redAway);
        dest.writeString(winner);
    }
}
