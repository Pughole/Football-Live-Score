package com.example.cobaapi;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface favoriteDAO {

    @Insert
    public void insert(TeamItem... TeamItem);
    @Update
    public void update(TeamItem... TeamItem);
    @Delete
    public void delete(TeamItem TeamItem);

    @Query("SELECT * FROM favorite where username = :username")
    List<TeamItem> getFavorites(String username);


}
