package com.example.cobaapi;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tUser")
public class user implements Parcelable {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "username")
    public String username;
    @ColumnInfo(name = "nama")
    public String nama;
    @ColumnInfo(name = "password")
    public String password;

    public user(String username, String nama, String password) {
        this.username = username;
        this.nama = nama;
        this.password = password;
    }

    protected user(Parcel in) {
        username = in.readString();
        nama = in.readString();
        password = in.readString();
    }

    public static final Creator<user> CREATOR = new Creator<user>() {
        @Override
        public user createFromParcel(Parcel in) {
            return new user(in);
        }

        @Override
        public user[] newArray(int size) {
            return new user[size];
        }
    };

    @NonNull
    public String getUsername() {
        return username;
    }

    public void setUsername(@NonNull String username) {
        this.username = username;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(nama);
        dest.writeString(password);
    }
}
