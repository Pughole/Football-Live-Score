package com.example.cobaapi;

public class matchplayer {
    private String imgp;
    private String namap;
    private String statp;
    private String teamp;
    private String descp;
    private String weightp;
    private String heightp;
    private String bornp;
    private String bplacep;
    private String nationp;

    public matchplayer(String imgp, String namap, String statp, String teamp, String descp, String weightp, String heightp, String bornp, String bplacep, String nationp) {
        this.imgp = imgp;
        this.namap = namap;
        this.statp = statp;
        this.teamp = teamp;
        this.descp = descp;
        this.weightp = weightp;
        this.heightp = heightp;
        this.bornp = bornp;
        this.bplacep = bplacep;
        this.nationp = nationp;
    }

    public String getImgp() {
        return imgp;
    }

    public String getNamap() {
        return namap;
    }

    public String getStatp() {
        return statp;
    }

    public String getTeamp() {
        return teamp;
    }

    public String getDescp() {
        return descp;
    }

    public String getWeightp() {
        return weightp;
    }

    public String getHeightp() {
        return heightp;
    }

    public String getBornp() {
        return bornp;
    }

    public String getBplacep() {
        return bplacep;
    }

    public String getNationp() {
        return nationp;
    }

    public void setImgp(String imgp) {
        this.imgp = imgp;
    }

    public void setNamap(String namap) {
        this.namap = namap;
    }

    public void setStatp(String statp) {
        this.statp = statp;
    }

    public void setTeamp(String teamp) {
        this.teamp = teamp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public void setWeightp(String weightp) {
        this.weightp = weightp;
    }

    public void setHeightp(String heightp) {
        this.heightp = heightp;
    }

    public void setBornp(String bornp) {
        this.bornp = bornp;
    }

    public void setBplacep(String bplacep) {
        this.bplacep = bplacep;
    }

    public void setNationp(String nationp) {
        this.nationp = nationp;
    }
}
