package com.example.cobaapi;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class AdapterRvMatchPlayer extends RecyclerView.Adapter<AdapterRvMatchPlayer.AdapterRvMatchPlayerViewHolder> {

    ArrayList<matchplayer> mp= new ArrayList<>();

    public AdapterRvMatchPlayer(ArrayList<matchplayer> mp) {
        this.mp = mp;
    }

    @NonNull
    @Override

    public AdapterRvMatchPlayerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View view= inflater.inflate(R.layout.playerinmatch,parent,false);
        return new AdapterRvMatchPlayer.AdapterRvMatchPlayerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRvMatchPlayerViewHolder holder, int position) {

        if  (mp.get(position).getTeamp()=="H"){
            holder.namaPH.setText(mp.get(position).getNamap());
            holder.statPH.setText(mp.get(position).getStatp());
        }
        if  (mp.get(position).getTeamp()=="A"){
            holder.namaPA.setText(mp.get(position).getNamap());
            holder.statPA.setText(mp.get(position).getStatp());
        }
    }

    @Override
    public int getItemCount() {
       return mp.size();
    }

    public class AdapterRvMatchPlayerViewHolder extends RecyclerView.ViewHolder {
        ImageView imPlayerH,imPlayerA;
        TextView namaPH, namaPA, statPH, statPA;
        public AdapterRvMatchPlayerViewHolder(@NonNull View itemView) {
            super(itemView);
            imPlayerH= itemView.findViewById(R.id.imPlayer);
            imPlayerA= itemView.findViewById(R.id.imPlayer);
            namaPH= itemView.findViewById(R.id.tvHplayer);
            namaPA= itemView.findViewById(R.id.tvHplayer);
            statPH= itemView.findViewById(R.id.statHplayer);
            statPA= itemView.findViewById(R.id.statHplayer);
        }
    }
}
