package com.example.cobaapi;

import org.json.JSONObject;

public class league {

    public String idLeague;
    public String strLeague;
    public String strLeagueAlternate;
    public String counrty;
    public String logo;
    public JSONObject object;

    public league(String idLeague, String strLeague, String strLeagueAlternate, String counrty, String logo){
        this.idLeague = idLeague;
        this.strLeague = strLeague;
        this.strLeagueAlternate = strLeagueAlternate;
    }

    public league(JSONObject object) {
        idLeague = "";
        strLeague = "";
        strLeagueAlternate = "";

        try {
            idLeague = object.getString("idLeague");

            strLeague = object.getString("strLeague");

            strLeagueAlternate = object.getString("strLeagueAlternate");

            counrty = object.getString("strCountry");

            logo = object.getString("strLogo");

            object = object;
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return strLeague;
    }

    public String getIdLeague() {
        return idLeague;
    }

    public void setIdLeague(String idLeague) {
        this.idLeague = idLeague;
    }

    public String getStrLeague() {
        return strLeague;
    }

    public void setStrLeague(String strLeague) {
        this.strLeague = strLeague;
    }

    public String getStrLeagueAlternate() {
        return strLeagueAlternate;
    }

    public void setStrLeagueAlternate(String strLeagueAlternate) {
        this.strLeagueAlternate = strLeagueAlternate;
    }

    public String getCounrty() {
        return counrty;
    }

    public void setCounrty(String counrty) {
        this.counrty = counrty;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
