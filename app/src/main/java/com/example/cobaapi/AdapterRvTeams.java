package com.example.cobaapi;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.InputStream;
import java.util.ArrayList;

public class AdapterRvTeams extends RecyclerView.Adapter<AdapterRvTeams.RvTeamsViewHolder>{
    ArrayList<TeamItem> listTeam = new ArrayList<TeamItem>();

    private OnItemClickCallback onItemClickCallback;

    public void setOnItemClickCallback(AdapterRvTeams.OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    public AdapterRvTeams(ArrayList<TeamItem> listTeam) {
        this.listTeam = listTeam;
    }

    @NonNull    @Override
    public AdapterRvTeams.RvTeamsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_team, parent, false);
        return new AdapterRvTeams.RvTeamsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterRvTeams.RvTeamsViewHolder holder, final int position) {
        holder.tvnama.setText(listTeam.get(position).getNama());
        new AdapterRvTeams.DownloadImageTask((ImageView) holder.logo)
                .execute(listTeam.get(position).getLogo());
        holder.itemView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                onItemClickCallback.onClick(
                        listTeam.get(holder.getAdapterPosition())
                );
            }
        });
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
            ImageView bmImage;

            public DownloadImageTask(ImageView bmImage) {
                this.bmImage = bmImage;
            }

            protected Bitmap doInBackground(String... urls) {
                String urldisplay = urls[0];
                Bitmap mIcon11 = null;
                try {
                    InputStream in = new java.net.URL(urldisplay).openStream();
                    mIcon11 = BitmapFactory.decodeStream(in);
                } catch (Exception e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
                return mIcon11;
            }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    @Override
    public int getItemCount() {
        return listTeam.size();
    }

    public class RvTeamsViewHolder extends RecyclerView.ViewHolder {
        TextView tvnama;
        ImageView logo;
        public RvTeamsViewHolder(@NonNull View itemView) {
            super(itemView);
            tvnama = itemView.findViewById(R.id.tv_titleTeam);
            logo=itemView.findViewById(R.id.logoTeam);
        }
    }

    public interface OnItemClickCallback{
        void onClick(TeamItem item);
    }
}
