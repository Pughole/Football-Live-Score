package com.example.cobaapi;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.json.JSONObject;

@Entity(tableName = "favorite")
public class TeamItem implements Parcelable {
    @PrimaryKey
    @NonNull
    private int id;
    private String nama,negara,venue,website,logo,username;


    public TeamItem(int id, String nama, String negara, String venue, String website, String logo, String username) {
        this.id = id;
        this.nama = nama;
        this.negara = negara;
        this.venue = venue;
        this.website = website;
        this.logo = logo;
        this.username = username;
    }

    public TeamItem(JSONObject object){
        try{
            nama ="";
            negara="";
            venue="";
            website="";
            logo="";

            nama = object.getString("strTeam");
            negara = object.getString("strCountry");
            venue = object.getString("strStadium");
            website=object.getString("strWebsite");
            logo=object.getString("strTeamBadge");
            id=object.getInt("idTeam");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    protected TeamItem(Parcel in) {
        id = in.readInt();
        nama = in.readString();
        negara = in.readString();
        venue = in.readString();
        website = in.readString();
        logo = in.readString();
        username = in.readString();
    }

    public static final Creator<TeamItem> CREATOR = new Creator<TeamItem>() {
        @Override
        public TeamItem createFromParcel(Parcel in) {
            return new TeamItem(in);
        }

        @Override
        public TeamItem[] newArray(int size) {
            return new TeamItem[size];
        }
    };

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNegara() {
        return negara;
    }

    public void setNegara(String negara) {
        this.negara = negara;
    }

    public String getVanue() {
        return venue;
    }

    public void setVanue(String vanue) {
        this.venue = vanue;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }


    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(nama);
        dest.writeString(negara);
        dest.writeString(venue);
        dest.writeString(website);
        dest.writeString(logo);
        dest.writeString(username);
    }
}
